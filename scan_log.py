import shutil
import os
import time
from version_check import *


class LogsCopy:
    def __init__(self, in_path, out_path, app, just_today):
        self.in_path = in_path
        self.out_path = out_path
        self.app = app
        self.just_today = just_today

    def run(self):
        source = os.path.join(self.in_path, self.app)
        target = os.path.join(self.out_path, self.app)

        if os.path.isdir(target):
            print(F"{target} is exist!!!")
            print(F"{target} is exist!!!")
            print(F"{target} is exist!!!")
            return 0

        if self.just_today == 1:
            self.all_logs(source, target)
        elif self.just_today == 2:
            self.today_logs(source, target)
        elif self.just_today == 3:
            date = input('Input Data EX:20190620 : ')
            self.select_logs(source, target, date)

    def all_logs(self, source, target):
        for root, dirs, files in os.walk(source):
            if "Logs" in dirs:
                print(F"Input <--- {root}")
                logs_source_path = os.path.join(root, 'Logs')
                print(logs_source_path)
                logs_target_path = os.path.relpath(root, source)
                shutil.copytree(logs_source_path, os.path.join(self.out_path, self.app, logs_target_path, "Logs"))
                print(F"Output ---> {os.path.join(self.out_path, self.app, logs_target_path)}")

    def select_logs(self, source, target, date):
        if not os.path.isdir(os.path.join(self.out_path, self.app)):
            os.makedirs(os.path.join(self.out_path, self.app))
        for root, dirs, files in os.walk(source):
            for filename in files:
                if "Logs" in root:
                    # print(os.path.join(root, filename))
                    logs_source_path = os.path.join(root, filename)
                    logs_target_path = os.path.relpath(root, source)
                    mtime = time.localtime(os.path.getmtime(logs_source_path))
                    file_modify_date = time.strftime("%Y%m%d", mtime)

                    if file_modify_date == date:
                        self.copy_one_file(filename, self.out_path, self.app,
                                           logs_source_path, logs_target_path, date='-' + file_modify_date)

    def today_logs(self, source, target):
        if not os.path.isdir(os.path.join(self.out_path, self.app)):
            os.makedirs(os.path.join(self.out_path, self.app))
        for root, dirs, files in os.walk(source):
            for filename in files:
                if "Logs" in root:
                    # print(os.path.join(root, filename))
                    if filename.endswith('.log'):
                        logs_source_path = os.path.join(root, filename)
                        logs_target_path = os.path.relpath(root, source)
                        self.copy_one_file(filename, self.out_path, self.app,
                                           logs_source_path, logs_target_path)

    def copy_one_file(self, filename, out_path, app, logs_source_path, logs_target_path, date = ''):
        # print(F"Input <--- {root}")
        print(F"Input <--- {logs_source_path}")
        try:
            out_put_file_name = logs_target_path.split('\\')[1]
        except:
            out_put_file_name = filename
        out_put_file_name += date
        shutil.copy2(logs_source_path, os.path.join(out_path, app, out_put_file_name))
        print(F"Output ---> {os.path.join(out_path,app, logs_target_path, out_put_file_name)}")


if __name__ == '__main__':
    url = 'https://crf8ze2osblrqrczzsvu7w-on.drv.tw/AppPointWorker/scan_log_version.json'
    try:
        PacketCheck('1.0.1', url).execute()
    except:
        print("PacketCheck error!!!")
    input_path = input('Please set Input Path (D:) : ') or os.path.join('D:', os.sep)
    output_path = input('Please set Output Path (D:/dirtypig) : ') or os.path.join('D:', os.sep, 'dirtypig')
    input_app = input('Please input app (APS) : ') or 'APS'
    just_today = int(input('Download (1)All logs (2)Today logs (3)Select date : ') or 1)
    LogsCopy(input_path, output_path, input_app, just_today).run()
