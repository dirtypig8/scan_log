import requests
import json
from threading import Thread
import os


class PacketCheck:
    def __init__(self, version, url):
        self.version = version
        self.url = url

    def execute(self):
        print("Now version : ", self.version)
        reqsjson = self.get_req()
        cloud_version = self.get_version(reqsjson)
        print("Cloud version : ", cloud_version)
        if not self.check_version(self.version, cloud_version)and \
                self.chcek_download_File_Exists(reqsjson[0]['file_name']):
            print("find new version")
            print("Auto download now...")
            download_file_from_google_drive(reqsjson[0]['file_ID'][33:], reqsjson[0]['file_name'])


    def get_req(self):
        reqs = requests.get(self.url)
        reqsjson = json.loads(reqs.text)
        return reqsjson

    def get_version(self, json):
        return json[0]['version']

    def check_version(self, now_version, new_version):
        if now_version == new_version:
            return 1
        else:
            return 0

    def chcek_download_File_Exists(self, file_name):
        savedirectory = os.getcwd()
        if os.path.isfile(os.path.join(savedirectory, file_name)):
            print(F"{file_name} is exist!!!")
            print(F"{file_name} is exist!!!")
            print(F"{file_name} is exist!!!")
            return 0
        else:
            return 1

def download_file_from_google_drive(id, destination):
    URL = "https://docs.google.com/uc?export=download"

    session = requests.Session()

    response = session.get(URL, params={'id': id}, stream=True)
    token = get_confirm_token(response)

    if token:
        params = {'id': id, 'confirm': token}
        response = session.get(URL, params=params, stream=True)

    save_response_content(response, destination)


def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith('download_warning'):
            print('download_warning')
            return value
    return None


def save_response_content(response, destination):
    CHUNK_SIZE = 32768

    with open(destination, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
    print('download_success')


if __name__ == '__main__':
    url = 'https://crf8ze2osblrqrczzsvu7w-on.drv.tw/AppPointWorker/scan_log_version.json'
    PacketCheck('1.0.0', url).execute()

    # version = '1.0.0'
    # print('now version : ', version)
    # url = 'https://crf8ze2osblrqrczzsvu7w-on.drv.tw/AppPointWorker/scan_log_version.json'
    # reqs = requests.get(url)
    # print(reqs.text)
    # reqsjson = json.loads(reqs.text)
    # print(reqsjson)
    # print('new version : ', reqsjson[0]['version'])
    # if version != reqsjson[0]['version']:
    #     print("Please Download New Version : v", reqsjson[0]['version'])
    #     print("update content : \n ", reqsjson[0]['content'])
    #     print(reqsjson[0]['path'])
